export enum MeasurementType {
  TEMP = "TEMP",
  HUMIDITY = "HUMIDITY",
  VOC = "VOC",
  CO2 = "CO2"
}