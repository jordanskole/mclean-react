import * as React from 'react';
import { Chart } from 'react-google-charts';
import { Query } from "react-apollo";
import gql from "graphql-tag";

import { Card, Spin } from 'antd';

import { MeasurementType } from '../../types/measurements'

interface LineChartProps {
  nearDate: Date
  farDate?: Date
  measurementTypes: [MeasurementType]
  sampleSize?: Number,
  title: string
  color?: string
}

const MEASUREMENT_QUERY = gql`
  query	MeasurementQuery(
    	$measurementType: MeasurementType,
    	$farDate: DateTime,
    	$nearDate: DateTime,
      $sampleSize: Int
  	){
    measurements(last: $sampleSize, where: {
      AND: [
        { measurementType: $measurementType}
        { status: PUBLISHED }
        { sensedOn_gte: $farDate }
        { sensedOn_lte: $nearDate }
      ]
    }) {
      id
      sensedOn
      value
    }
  }
`

export const LineChart: React.SFC<LineChartProps> = ({ nearDate, farDate, measurementTypes, sampleSize = 15, title, color = 'blue'}) => {
  farDate = farDate ? farDate : new Date(nearDate.getDate() - 30)
  const variables = {
    sampleSize,
    "measurementType": measurementTypes[0],
    "nearDate": nearDate.toISOString(),
    "farDate": farDate.toISOString(),
  }
  return (
    <Card title={title} style={{marginBottom: '20px'}}>
      <Query query={MEASUREMENT_QUERY} variables={variables}>
        {({loading, error, data: { measurements }}) => {

          if (loading) return null;
          if (error) return <div>Something went wrong</div>

          const formattedData = measurements.map(({sensedOn, value} : any) => [new Date(sensedOn), value])

          return (
            <Chart
              // width={'100%'}
              height={400}
              chartType="LineChart"
              loader={<Spin />}
              data={[[
                  {label: 'Sensed On', type: 'datetime'},
                  {label: measurementTypes[0], type: 'number'}
                ],
                ...formattedData
              ]}
              options={{
                curveType: 'function',
                chartArea: { width: '88%', height: '80%' },
                lineWidth: 2,
                legend: { position: 'top' },
                series: { 0: { color }}
              }}
              rootProps={{ 'data-testid': '1' }}
            />
          )
        }}
      </Query>
    </Card>
  );
}
