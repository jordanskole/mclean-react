import React, { Component } from 'react';
import { ApolloClient } from "apollo-client";
import { createHttpLink } from "apollo-link-http";
import { InMemoryCache } from "apollo-cache-inmemory";
import { ApolloProvider } from "react-apollo";

import { Row, Col } from 'antd';
import { Helmet } from "react-helmet";

import { LineChart } from './components/charts'
import { MeasurementType } from './types/measurements'

import './App.css';

export const link = createHttpLink({
  uri: "https://api-useast.graphcms.com/v1/cjpioi84x04wm01h0q436mxv6/master"
});

export const client = new ApolloClient({
  cache: new InMemoryCache(),
  link,
});

class App extends Component {
  render() {
    const near = new Date()
    const far = new Date()
    far.setDate(far.getDate() - 30)

    return (
      <ApolloProvider client={client}>
        <Helmet>
          <meta charSet="utf-8" />
          <title>McLean by the numbers</title>
          <link rel="canonical" href="http://mclean.ventures-adventures.com" />
        </Helmet>
        <Row gutter={16} style={{padding: '20px'}}>
          <Col xs={24} sm={24} md={{span: 20, offset: 2}} lg={{span: 12, offset: 0}} xxl={8}>
            <LineChart
              title="Temperature"
              nearDate={near}
              farDate={far}
              measurementTypes={[MeasurementType.TEMP]}
              sampleSize={15}
              color='red'
            />
          </Col>
          <Col xs={24} sm={24} md={{span: 20, offset: 2}} lg={{span: 12, offset: 0}} xxl={8}>
            <LineChart
              title="Humidity"
              nearDate={near}
              farDate={far}
              measurementTypes={[MeasurementType.HUMIDITY]}
              sampleSize={15}
            />
          </Col>
        </Row>
      </ApolloProvider>
    );
  }
}

export default App;
